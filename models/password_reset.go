package models

type (
	PasswordResetRequest struct {
		Token    string `json:"token"`
		Password string `json:"password"`
	}
	EmailResponse struct {
		Email string `json:"email"`
	}
	GeneratePasswordResetRequest struct {
		Email    string `json:"email"`
		HostName string `json:"host_name"`
	}
)
