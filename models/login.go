package models

type (
	LoginRequest struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}
	GoogleLoginRequest struct {
		GoogleID    string `json:"google_id"`
		GoogleEmail string `json:"google_email"`
	}
	LoginResponse struct {
		Token string `json:"token"`
	}
	AuthToken struct {
		Token string `json:"token"`
	}
)
