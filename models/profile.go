package models

type Profile struct {
	UserProfileID int64  `json:"user_profile_id"`
	FullName      string `json:"full_name"`
	Address       string `json:"address"`
	Telephone     string `json:"telephone"`
	Email         string `json:"email"`
	Password      string `json:"password"`
}
