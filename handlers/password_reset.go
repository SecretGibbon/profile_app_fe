package handlers

import (
	"SecretGibbon/profileApp/FE/api"
	"SecretGibbon/profileApp/FE/config"
	"SecretGibbon/profileApp/FE/models"
	"encoding/base64"
	"encoding/json"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
)

type (
	PasswordResetData struct {
		Token         string
		Email         string
		Password      string
		PasswordCheck string
		Errors        map[string]string
	}
	ResetPasswordQueryData struct {
		Email  string
		Errors map[string]string
	}
)

func ResetPasswordForm(w http.ResponseWriter, r *http.Request) {
	prd := PasswordResetData{
		Errors: make(map[string]string),
	}
	defer func() {
		err := renderTemplate(w, "pages/password_reset.html", prd)
		if err != nil {
			log.Printf("[ERROR] Password reset page render error: %e", err)
			http.Error(w, "Error rendering password reset page", http.StatusInternalServerError)
		}
	}()
	token, ok := mux.Vars(r)["token"]
	if !ok {
		return
	}
	prd.Token = token

	response, err := api.CallAPI("reset_password/"+token, http.MethodGet, nil)
	if err != nil {
		prd.Errors["Form"] = "Failed to call API server"
		return
	}
	if response.StatusCode == http.StatusNotFound {
		prd.Errors["Form"] = "Token is not valid"
		return
	}
	if response.StatusCode == http.StatusPreconditionFailed {
		prd.Errors["Form"] = "Token is expired"
		return
	}
	if response.StatusCode != http.StatusOK {
		prd.Errors["Form"] = "Unknown error"
		return
	}

	emailResponseBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return
	}

	var emailResponse models.EmailResponse
	err = json.Unmarshal(emailResponseBytes, &emailResponse)
	if err != nil {
		prd.Errors["Form"] = "Failed to read response from API server"
		return
	}

	prd.Email = emailResponse.Email
}

func ResetPassword(w http.ResponseWriter, r *http.Request) {
	prd := PasswordResetData{
		Token:         r.PostFormValue("token"),
		Email:         r.PostFormValue("email"),
		Password:      r.PostFormValue("password"),
		PasswordCheck: r.PostFormValue("password_check"),
		Errors:        make(map[string]string),
	}
	if prd.Password != prd.PasswordCheck {
		prd.Errors["Form"] = "Passwords do not match"
		err := renderTemplate(w, "pages/password_reset.html", prd)
		if err != nil {
			log.Printf("[ERROR] Password reset page render error: %e", err)
			http.Error(w, "Error rendering password reset page", http.StatusInternalServerError)
		}
		return
	}

	encodedPassword := base64.StdEncoding.EncodeToString([]byte(prd.Password + "_" + prd.Email))
	requestData := models.PasswordResetRequest{
		Token:    prd.Token,
		Password: encodedPassword,
	}
	response, err := api.CallAPIEncrypted("reset_password", http.MethodPost, requestData)
	if err != nil {
		return
	}

	if response.StatusCode != http.StatusOK {
		if response.StatusCode == http.StatusNotFound {
			prd.Errors["Form"] = "Token is not valid"
		} else {
			prd.Errors["Form"] = "Api server failed to reset password"
		}
		err := renderTemplate(w, "pages/password_reset.html", prd)
		if err != nil {
			log.Printf("[ERROR] Password reset page render error: %e", err)
			http.Error(w, "Error rendering password reset page", http.StatusInternalServerError)
		}
		return
	}

	lpd := LoginPageData{
		GoogleAuthClientID: config.GoogleAuthClientID,
	}
	err = renderTemplate(w, "pages/login.html", lpd)
	if err != nil {
		log.Printf("[ERROR] Password reset page render error: %e", err)
		http.Error(w, "Error rendering password reset page", http.StatusInternalServerError)
	}
}

func ResetPasswordQuery(w http.ResponseWriter, r *http.Request) {
	prd := ResetPasswordQueryData{}
	err := renderTemplate(w, "pages/password_reset_query.html", prd)
	if err != nil {
		log.Printf("[ERROR] Password reset query page render error: %e", err)
		http.Error(w, "Error rendering password reset query page", http.StatusInternalServerError)
	}
}

func GeneratePasswordReset(w http.ResponseWriter, r *http.Request) {
	prd := ResetPasswordQueryData{
		Email:  r.PostFormValue("email"),
		Errors: make(map[string]string),
	}
	defer func() {
		err := renderTemplate(w, "pages/password_reset_query.html", prd)
		if err != nil {
			log.Printf("[ERROR] Password reset query page render error: %e", err)
			http.Error(w, "Error rendering password reset query page", http.StatusInternalServerError)
		}
	}()
	generatePasswordResetRequest := models.GeneratePasswordResetRequest{
		Email:    prd.Email,
		HostName: config.ExternalAddr,
	}
	response, err := api.CallAPI("generate_password_reset", http.MethodPost, generatePasswordResetRequest)
	if err != nil {
		prd.Errors["Form"] = "Failed to call API server"
		return
	}

	if response.StatusCode == http.StatusNotFound {
		prd.Errors["Form"] = "Given email is not valid"
		return
	}
	if response.StatusCode == http.StatusUnprocessableEntity {
		prd.Errors["Form"] = "Cannot reset password for Google authenticated users"
		return
	}
	if response.StatusCode != http.StatusOK {
		prd.Errors["Form"] = "API server failed to send password reset e-mail"
		return
	}

	prd.Errors["Form"] = "Email was sent to your address, please follow the link to reset your password"
}
