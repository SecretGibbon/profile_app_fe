package handlers

import (
	"SecretGibbon/profileApp/FE/api"
	"SecretGibbon/profileApp/FE/config"
	"SecretGibbon/profileApp/FE/models"
	"SecretGibbon/profileApp/FE/utils"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
)

type ProfilePageData struct {
	Disconnect         bool
	GoogleAuthClientID string
	IsRegister         bool
	IsEditable         bool
	GoogleID           string
	Token              string
	Profile            models.Profile
	Errors             map[string]string
}

func UpdateProfile(w http.ResponseWriter, r *http.Request) {
	ppd := ProfilePageData{
		GoogleAuthClientID: config.GoogleAuthClientID,
		GoogleID:           r.PostFormValue("google_id"),
		Token:              r.PostFormValue("token"),
		IsEditable:         r.PostFormValue("is_editable") == "true",
		Profile: models.Profile{
			FullName:  r.PostFormValue("full_name"),
			Address:   r.PostFormValue("address"),
			Telephone: r.PostFormValue("telephone"),
			Email:     r.PostFormValue("email"),
			Password:  r.PostFormValue("password"),
		},
		Errors: make(map[string]string),
	}

	defer func() {
		err := renderTemplate(w, "pages/profile.html", ppd)
		if err != nil {
			log.Printf("[ERROR] Profile page render error: %e", err)
			http.Error(w, "Error rendering profile page", http.StatusInternalServerError)
		}
	}()

	if r.PostFormValue("cancel_button") == "Cancel" {
		storedProfile, err := getStoredProfile(ppd.Token)
		if err != nil {
			ppd.Errors["Form"] = "Failed to retrieve profile data"
			return
		}
		ppd.Profile = storedProfile
		ppd.Profile.Password = ""
		ppd.IsEditable = false
		return
	}

	if !ppd.IsEditable {
		storedProfile, err := getStoredProfile(ppd.Token)
		if err != nil {
			ppd.Errors["Form"] = "Failed to retrieve profile data"
			return
		}
		ppd.Profile = storedProfile
		ppd.Profile.Password = ""
		ppd.IsEditable = true
		return
	}

	if ppd.GoogleID != "" {
		ppd.Profile.Email = r.PostFormValue("google_email")
	}

	userProfileIDString := r.PostFormValue("user_profile_id")
	userProfileID, err := strconv.ParseInt(userProfileIDString, 10, 64)
	if err != nil {
		log.Printf("[ERROR] Profile page error: %e", err)
		ppd.Errors["Form"] = "Failed parsing user_profile_id from profile data"
		return
	}
	ppd.Profile.UserProfileID = userProfileID

	oldProfile, err := getStoredProfile(ppd.Token)
	if err != nil {
		log.Printf("[ERROR] Failed to retreive stored profile: %e", err)
		ppd.Errors["Form"] = "Error retrieving stored profile data"
		return
	}

	if ppd.GoogleID == "" && (ppd.Profile.Password != "" || ppd.Profile.Email != oldProfile.Email) {
		if ppd.Profile.Password == "" {
			oldPasswordBytes, err := base64.StdEncoding.DecodeString(oldProfile.Password)
			if err != nil {
				log.Printf("[ERROR] Profile page error: %e", err)
				ppd.Errors["Form"] = "Failed decoding old password"
				return
			}
			plainOldPassword := strings.TrimSuffix(string(oldPasswordBytes), "_"+oldProfile.Email)
			ppd.Profile.Password = plainOldPassword
		}
		encodedPassword := base64.StdEncoding.EncodeToString([]byte(ppd.Profile.Password + "_" + ppd.Profile.Email))
		ppd.Profile.Password = encodedPassword
	} else {
		ppd.Profile.Password = oldProfile.Password
	}

	ppd.Profile.UserProfileID = oldProfile.UserProfileID
	updateProfileResponse, err := api.CallAPIWithToken("profile", http.MethodPatch, ppd.Token, ppd.Profile)
	if err != nil {
		return
	}

	if updateProfileResponse.StatusCode != http.StatusOK {
		ppd.Errors["Form"] = "Response from server not OK"
	}

	ppd.Profile.Password = ""
	ppd.IsEditable = false
}

func getStoredProfile(token string) (profile models.Profile, err error) {
	response, err := api.CallAPIWithToken("profile", http.MethodGet, token, nil)
	if err != nil {
		return
	}

	if response.StatusCode != http.StatusOK {
		return profile, fmt.Errorf("server did not responded with OK")
	}

	profileBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return
	}

	err = json.Unmarshal(profileBytes, &profile)
	return
}

func RegisterForm(w http.ResponseWriter, r *http.Request) {
	ppd := ProfilePageData{
		GoogleAuthClientID: config.GoogleAuthClientID,
		IsRegister:         true,
		IsEditable:         true,
	}
	err := renderTemplate(w, "pages/profile.html", ppd)
	if err != nil {
		log.Printf("[ERROR] Profile page render error: %e", err)
		http.Error(w, "Error rendering profile page", http.StatusInternalServerError)
	}
}

func RegisterNewUser(w http.ResponseWriter, r *http.Request) {
	plainPassword := r.PostFormValue("password")
	ppd := ProfilePageData{
		GoogleAuthClientID: config.GoogleAuthClientID,
		GoogleID:           r.PostFormValue("google_id"),
		Profile: models.Profile{
			FullName:  r.PostFormValue("full_name"),
			Address:   r.PostFormValue("address"),
			Telephone: r.PostFormValue("telephone"),
			Email:     r.PostFormValue("email"),
		},
		Errors: make(map[string]string),
	}
	if ppd.GoogleID != "" {
		ppd.Disconnect = false
		ppd.Profile.FullName = ""
		ppd.Profile.Address = ""
		ppd.Profile.Telephone = ""
		ppd.Profile.Password = ""
		ppd.Profile.Email = r.PostFormValue("google_email")
	} else {
		encodedPassword := base64.StdEncoding.EncodeToString([]byte(plainPassword + "_" + ppd.Profile.Email))
		ppd.Profile.Password = encodedPassword
	}

	defer func() {
		err := renderTemplate(w, "pages/profile.html", ppd)
		if err != nil {
			log.Printf("[ERROR] Profile page render error: %e", err)
			http.Error(w, "Error rendering profile page", http.StatusInternalServerError)
		}
	}()

	var response *http.Response
	var err error
	if ppd.GoogleID == "" {
		response, err = api.CallAPIEncrypted("profile", http.MethodPut, ppd.Profile)
	} else {
		requestData := models.GoogleLoginRequest{
			GoogleID:    ppd.GoogleID,
			GoogleEmail: ppd.Profile.Email,
		}
		response, err = api.CallAPIEncrypted("google_profile", http.MethodPut, requestData)
	}
	validationMessage := validateCreateResponse(response, err)
	if validationMessage != "" {
		ppd.Errors["Form"] = validationMessage
		ppd.IsRegister = true
		ppd.IsEditable = true
		ppd.Profile.FullName = ""
		ppd.Profile.Address = ""
		ppd.Profile.Telephone = ""
		ppd.Profile.Email = ""
		ppd.GoogleID = ""
		ppd.Profile.Password = ""
		ppd.Disconnect = true
		return
	}

	encryptedBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		ppd.Errors["Form"] = "Failed to decrypt API server response"
		return
	}

	loginResponseJson, err := utils.Decrypt(encryptedBytes)
	if err != nil {
		ppd.Errors["Form"] = "Failed to decrypt API server response"
		return
	}

	var loginResponse models.LoginResponse
	err = json.Unmarshal(loginResponseJson, &loginResponse)
	if err != nil {
		ppd.Errors["Form"] = "Failed to decrypt API server response"
		return
	}

	encryptedToken, err := encryptToken(loginResponse.Token)
	if err != nil {
		log.Printf("[ERROR] Failed to encrypt token: %e", err)
		ppd.Errors["Form"] = "Failed to encrypt received token"
		return
	}

	ppd.Token = encryptedToken
	ppd.IsRegister = false
	if ppd.GoogleID != "" {
		ppd.IsEditable = true
	} else {
		ppd.IsEditable = false
	}
}

func validateCreateResponse(response *http.Response, responseErr error) (validationMessage string) {
	if responseErr != nil {
		log.Printf("[ERROR] Api call returned error %e", responseErr)
		return "Failed to call API server"
	}

	if response.StatusCode == http.StatusPreconditionFailed {
		log.Printf("User with this email already exists")
		return "User with this account already exists"
	}

	if response.StatusCode != http.StatusCreated {
		log.Printf("[ERROR] Api did not return 201 - Created")
		return "api failed to create user profile"
	}
	return ""
}
