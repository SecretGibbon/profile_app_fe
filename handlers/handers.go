package handlers

import (
	"SecretGibbon/profileApp/FE/models"
	"SecretGibbon/profileApp/FE/utils"
	"encoding/base64"
	"encoding/json"
	"html/template"
	"net/http"
)

func renderTemplate(w http.ResponseWriter, templatePath string, params interface{}) (err error) {
	t, err := template.ParseFiles(templatePath)
	if err != nil {
		return
	}
	err = t.Execute(w, params)
	return
}

func encryptToken(token string) (result string, err error) {
	authToken := models.AuthToken{
		Token: token,
	}
	authTokenBytes, err := json.Marshal(authToken)
	if err != nil {
		return
	}

	encryptedToken, err := utils.Encrypt(authTokenBytes)
	if err != nil {
		return
	}

	base64EncryptedToken := base64.StdEncoding.EncodeToString(encryptedToken)
	result = base64.StdEncoding.EncodeToString([]byte(base64EncryptedToken + ":"))
	return
}
