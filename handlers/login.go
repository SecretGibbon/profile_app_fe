package handlers

import (
	"SecretGibbon/profileApp/FE/api"
	"SecretGibbon/profileApp/FE/config"
	"SecretGibbon/profileApp/FE/models"
	"SecretGibbon/profileApp/FE/utils"
	"encoding/base64"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

type LoginPageData struct {
	GoogleID           string
	GoogleEmail        string
	GoogleAuthClientID string
	Username           string
	Password           string
	Errors             map[string]string
	Token              string
}

func LoginPage(w http.ResponseWriter, r *http.Request) {
	lpd := LoginPageData{
		GoogleAuthClientID: config.GoogleAuthClientID,
	}

	err := renderTemplate(w, "pages/login.html", lpd)
	if err != nil {
		log.Printf("[ERROR] Login page render error: %e", err)
		_, err = w.Write([]byte("Error rendering login page"))
		return
	}
}

func (lpd *LoginPageData) doLogin() {
	lpd.Errors = make(map[string]string)

	encodedPassword := base64.StdEncoding.EncodeToString([]byte(lpd.Password + "_" + lpd.Username))
	loginRequest := models.LoginRequest{
		Username: lpd.Username,
		Password: encodedPassword,
	}

	response, err := api.CallAPIEncrypted("login", http.MethodPost, loginRequest)
	if err != nil {
		lpd.Errors["Form"] = "Failed to call API server"
		return
	}

	if response.StatusCode == http.StatusBadRequest {
		lpd.Errors["Form"] = "Cannot use password login for Google authenticated user"
		return
	}

	if response.StatusCode == http.StatusUnauthorized {
		lpd.Errors["Form"] = "Wrong password"
		return
	}

	if response.StatusCode == http.StatusNotFound {
		lpd.Errors["Form"] = "User does not exist"
		return
	}

	if response.StatusCode == http.StatusConflict {
		lpd.Errors["Form"] = "User is registered by google account. Please use Google Sign-in"
		return
	}

	if response.StatusCode != http.StatusOK {
		lpd.Errors["Form"] = "Login failed"
		return
	}

	encryptedResponse, err := ioutil.ReadAll(response.Body)
	if err != nil {
		lpd.Errors["Form"] = "Failed to read data from API server"
		return
	}
	result, err := utils.Decrypt(encryptedResponse)
	if err != nil {
		lpd.Errors["Form"] = "Failed to decrypt server response"
		return
	}

	var loginResponse models.LoginResponse
	err = json.Unmarshal(result, &loginResponse)
	if err != nil {
		lpd.Errors["Form"] = "Failed to decrypt server response"
		return
	}

	encryptedToken, err := encryptToken(loginResponse.Token)
	if err != nil {
		log.Printf("[ERROR] Failed to encrypt token: %e", err)
		lpd.Errors["Form"] = "Failed to encrypt received token"
		return
	}

	lpd.Token = encryptedToken

	return
}

func (lpd *LoginPageData) doGoogleLogin() (newProfile bool) {
	lpd.Errors = make(map[string]string)

	googleLoginRequest := models.GoogleLoginRequest{
		GoogleID:    lpd.GoogleID,
		GoogleEmail: lpd.GoogleEmail,
	}

	response, err := api.CallAPIEncrypted("google_login", http.MethodPost, googleLoginRequest)
	if err != nil {
		lpd.Errors["Form"] = "Failed to call API server"
		return
	}

	if response.StatusCode == http.StatusNotFound {
		lpd.Errors["Form"] = "User is not registered"
		return
	}

	if response.StatusCode == http.StatusConflict {
		lpd.Errors["Form"] = "User is registered as password user. Please use password authentication"
		return
	}

	if response.StatusCode != http.StatusOK {
		lpd.Errors["Form"] = "Login failed"
		return
	}

	encryptedResponse, err := ioutil.ReadAll(response.Body)
	if err != nil {
		lpd.Errors["Form"] = "Failed to read data from API server"
		return
	}
	result, err := utils.Decrypt(encryptedResponse)
	if err != nil {
		lpd.Errors["Form"] = "Failed to decrypt server response"
		return
	}

	var loginResponse models.LoginResponse
	err = json.Unmarshal(result, &loginResponse)
	if err != nil {
		lpd.Errors["Form"] = "Failed to decrypt server response"
		return
	}

	authToken := models.AuthToken{
		Token: loginResponse.Token,
	}
	authTokenBytes, err := json.Marshal(authToken)
	if err != nil {
		lpd.Errors["Form"] = "Failed to parse token from server response"
		return
	}

	encryptedToken, err := utils.Encrypt(authTokenBytes)
	if err != nil {
		lpd.Errors["Form"] = "Failed to encrypt received token"
		return
	}

	base64EncryptedToken := base64.StdEncoding.EncodeToString(encryptedToken)
	base64EncryptedToken = base64.StdEncoding.EncodeToString([]byte(base64EncryptedToken + ":"))
	lpd.Token = base64EncryptedToken

	return response.StatusCode == http.StatusCreated
}

func DoLogin(w http.ResponseWriter, r *http.Request) {
	lpd := &LoginPageData{
		GoogleID:           r.PostFormValue("google_id"),
		GoogleEmail:        r.PostFormValue("google_email"),
		GoogleAuthClientID: config.GoogleAuthClientID,
		Username:           r.PostFormValue("username"),
		Password:           r.PostFormValue("password"),
	}

	newGoogleProfile := false
	if lpd.GoogleID != "" {
		newGoogleProfile = lpd.doGoogleLogin()
	} else {
		lpd.doLogin()
	}

	if len(lpd.Errors) != 0 {
		returnToLogin(w, lpd)
		return
	}

	storedProfile, err := getStoredProfile(lpd.Token)
	if err != nil {
		lpd.Errors["Form"] = "Failed to retrieve profile data"
		returnToLogin(w, lpd)
		return
	}

	var ppd ProfilePageData
	ppd.GoogleAuthClientID = config.GoogleAuthClientID
	ppd.Profile = storedProfile
	ppd.Token = lpd.Token
	ppd.Profile.Password = ""
	ppd.GoogleID = lpd.GoogleID
	if newGoogleProfile {
		ppd.IsEditable = true
	}

	err = renderTemplate(w, "pages/profile.html", ppd)
	if err != nil {
		log.Printf("[ERROR] Profile page render error: %e", err)
		http.Error(w, "Error rendering login page", http.StatusInternalServerError)
		return
	}
}

func returnToLogin(w http.ResponseWriter, lpd *LoginPageData) {
	lpd.GoogleID = ""
	lpd.GoogleEmail = ""
	err := renderTemplate(w, "pages/login.html", lpd)
	if err != nil {
		log.Printf("[ERROR] Login page error: %e", err)
		http.Error(w, "Error rendering login page", http.StatusInternalServerError)
	}
}
