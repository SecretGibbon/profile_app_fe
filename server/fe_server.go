package server

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"

	"SecretGibbon/profileApp/FE/config"
	"SecretGibbon/profileApp/FE/handlers"
)

func Start() {
	config.LoadEnvParams()
	router := createRouter()
	fmt.Println("Server started on " + config.ListenAddr)
	log.Fatal(http.ListenAndServe(config.ListenAddr, router))
}

func createRouter() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/", handlers.LoginPage).Methods("GET")
	router.HandleFunc("/", handlers.DoLogin).Methods("POST")
	//router.HandleFunc("/profile", handlers.ShowProfile).Methods("GET")
	router.HandleFunc("/profile", handlers.UpdateProfile).Methods("POST")
	router.HandleFunc("/register", handlers.RegisterForm).Methods("GET")
	router.HandleFunc("/register", handlers.RegisterNewUser).Methods("POST")
	router.HandleFunc("/password_reset/{token}", handlers.ResetPasswordForm).Methods("GET")
	router.HandleFunc("/password_reset", handlers.ResetPassword).Methods("POST")
	router.HandleFunc("/password_reset_query", handlers.ResetPasswordQuery).Methods("GET")
	router.HandleFunc("/password_reset_query", handlers.GeneratePasswordReset).Methods("POST")
	return router
}
