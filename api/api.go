package api

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"time"

	"SecretGibbon/profileApp/FE/config"
	"SecretGibbon/profileApp/FE/utils"
)

func CallAPIWithToken(path, method, token string, data interface{}) (response *http.Response, err error) {
	request, err := prepareRequestWithToken(method, "http://"+config.APIHostname+":"+config.APIPort+"/"+path, token, getRequestBody(data, false))
	if err != nil {
		return
	}
	client := http.Client{
		Timeout: 5 * time.Second,
	}
	response, err = client.Do(request)
	return
}

func CallAPIEncrypted(path, method string, data interface{}) (response *http.Response, err error) {
	request, err := prepareRequest(method, "http://"+config.APIHostname+":"+config.APIPort+"/"+path, getRequestBody(data, true))
	if err != nil {
		return
	}

	client := http.Client{
		Timeout: 5 * time.Second,
	}
	response, err = client.Do(request)
	return
}

func CallAPI(path, method string, data interface{}) (response *http.Response, err error) {
	request, err := prepareRequest(method, "http://"+config.APIHostname+":"+config.APIPort+"/"+path, getRequestBody(data, false))
	if err != nil {
		return
	}

	client := http.Client{
		Timeout: 5 * time.Second,
	}
	response, err = client.Do(request)
	return
}

func getRequestBody(data interface{}, encrypt bool) (body io.Reader) {
	if data != nil {
		dataBytes, err := json.Marshal(data)
		if err != nil {
			log.Printf("Failed to marshall request body, sending request without body")
			return
		}
		if encrypt {
			dataBytes, err = utils.Encrypt(dataBytes)
			if err != nil {
				log.Printf("Failed to encrypt request body, sending request without body")
				return
			}
		}
		body = bytes.NewBuffer(dataBytes)
	}
	return
}

func prepareRequest(method, url string, body io.Reader) (request *http.Request, err error) {
	request, err = http.NewRequest(method, url, body)
	if err != nil {
		return
	}
	if body != nil {
		request.Header.Set("Content-Type", "application/json")
	}
	return
}

func prepareRequestWithToken(method, url, token string, body io.Reader) (request *http.Request, err error) {
	request, err = http.NewRequest(method, url, body)
	if err != nil {
		return
	}
	if body != nil {
		request.Header.Set("Content-Type", "application/json")
	}
	request.Header.Set("Authorization", "Basic "+token)
	return
}
