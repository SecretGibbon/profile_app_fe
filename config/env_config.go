package config

import (
	"errors"
	"log"
	"os"

	"github.com/namsral/flag"
)

var (
	ListenAddr         string
	ExternalAddr       string
	APIHostname        string
	APIPort            string
	GoogleAuthClientID string
	EncryptionKey      string
)

func LoadEnvParams() {
	fs := flag.NewFlagSetWithEnvPrefix(os.Args[0], "PROFILE_APP_FE", flag.ExitOnError)
	fs.StringVar(&ListenAddr, "listen", "127.0.0.1:80", "TCP listen address/port (default 127.0.0.1:80)")
	fs.StringVar(&ExternalAddr, "external_addr", "0.0.0.0:80", "External address (default 0.0.0.0:80)")
	fs.StringVar(&APIHostname, "api_hostname", "localhost", "Hostname of \"user:password@(hostname:3306)/database_name\"")
	fs.StringVar(&APIPort, "api_port", "8080", "Port of the API")
	fs.StringVar(&GoogleAuthClientID, "google_auth_client_id", "", "Client ID for google auth authentication")
	fs.StringVar(&EncryptionKey, "encryption_key", "", "Encryption key for safe credentials processing")
	flag.Parse()

	var err error
	if err = fs.Parse(os.Args[1:]); err != nil {
		log.Fatalf("Error parsing arguments %+v: %s\n", os.Args, err.Error())
	}

	if err = validateCfgParams(); err != nil {
		log.Fatalf("Invalid environment configuration: %s", err.Error())
	}

	return
}

func validateCfgParams() error {
	if APIHostname == "" {
		return errors.New("empty APIHostname")
	}
	if APIPort == "" {
		return errors.New("empty APIport")
	}
	if GoogleAuthClientID == "" {
		return errors.New("empty GoogleAuth ClientID")
	}
	if EncryptionKey == "" {
		return errors.New("empty encryption_key")
	}
	return nil
}
